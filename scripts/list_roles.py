import anime_list as al
import seiyuu_list as sl

def list_roles(seiyuu_id):
    sl.get_seiyuu_list()
    
    if seiyuu_id not in sl.seiyuu_idx:
        print('seiyuu id not in database')
        return
    
    sl.get_seiyuu_db()
    al.get_anime_order()
    al.get_anime_db()
    
    seiyuu = sl.seiyuu_db[sl.seiyuu_idx[seiyuu_id]]
    seiyuu.roles.sort(key=lambda role : al.anime_idx[role[0]], reverse=True)
    
    print('Roles for {} ({}):'.format(seiyuu.name, seiyuu.id))
    for anime_id, character_name in seiyuu.roles:
        print()
        print(' {}'.format(character_name))
        print(' {} ({})'.format(al.anime_db[al.anime_inv[anime_id]].title, anime_id))

def main():
    import sys
    seiyuu_id = sys.argv[1]
    list_roles(seiyuu_id)
    return

if __name__ == '__main__':
    main()
