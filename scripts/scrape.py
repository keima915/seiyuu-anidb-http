import sys
import subprocess
import config as cf
import cache_list as cl

def download(anime_id):
    cl.get_cache_list()
    if anime_id in cl.cache_list:
        return
    cl.add_to_cache(anime_id)
    
    cf.read_config()
    WGET_COMMAND = 'wget \'http://api.anidb.net:9001/httpapi?request=anime&client={}&clientver={}&protover=1&aid={}\' -O cache/{}.gzip'
    subprocess.run(WGET_COMMAND.format(cf.client, cf.clientver, anime_id, anime_id), shell=True)
    subprocess.run('7z x cache/{}.gzip -ocache'.format(anime_id), shell=True)
    subprocess.run('rm cache/{}.gzip'.format(anime_id), shell=True)

def get_title(anime_id):
    sys.stdin = open('cache/{}'.format(anime_id), 'r')
    s = sys.stdin.read()
    sys.stdin.close()
    
    title = s.split('<title xml:lang="ja" type="official">')[1].split('</title>')[0]
    
    return title

class Character:
    def __init__(self, name, seiyuu_name, seiyuu_id):
        self.name = name
        self.seiyuu_name = seiyuu_name
        self.seiyuu_id = seiyuu_id

def get_characters(anime_id):
    sys.stdin = open('cache/{}'.format(anime_id), 'r')
    s = sys.stdin.read()
    sys.stdin.close()
    
    arr = [t.split('</character>')[0] for t in s.split('<character ')[1:]]
    
    characters = []
    
    for character in arr:
        if '<seiyuu ' not in character:
            continue
        
        name = character.split('<name>')[1].split('</name>')[0]
        seiyuu_name = character.split('</seiyuu>')[0].split('>')[-1]
        seiyuu_id = character.split('<seiyuu id="')[1].split('"')[0]
        
        characters.append(Character(name, seiyuu_name, seiyuu_id))
    
    return characters

def main():
    return

if __name__ == '__main__':
    main()
