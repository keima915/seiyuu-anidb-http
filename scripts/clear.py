import sys

def clear():
    sys.stdout = open('db/anime', 'w')
    print('0\n\n', end='')
    sys.stdout.close()
    
    sys.stdout = open('db/anime_order', 'w')
    print('0\n\n', end='')
    sys.stdout.close()
    
    sys.stdout = open('db/seiyuu', 'w')
    print('0\n\n', end='')
    sys.stdout.close()
    
    sys.stdout = open('db/seiyuu_list', 'w')
    print('0\n\n', end='')
    sys.stdout.close()

def main():
    clear()
    return

if __name__ == '__main__':
    main()
