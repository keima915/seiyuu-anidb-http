import anime_list as al
import seiyuu_list as sl

def list_seiyuu_anime(anime_id):
    al.get_anime_order()
    
    if anime_id not in al.anime_idx:
        print('anime id not in database')
        return
    
    sl.get_seiyuu_db()
    sl.get_seiyuu_list()
    al.get_anime_db()
    
    characters = al.anime_db[al.anime_inv[anime_id]].characters
    characters.sort(key=lambda character: len(sl.seiyuu_db[sl.seiyuu_idx[character[0]]].anime), reverse=True)
    
    print('Characters in {}:'.format(al.anime_db[al.anime_inv[anime_id]].title))
    for character in characters:
        print()
        print(' {}'.format(character[1]))
        print(' {} ({})'.format(sl.seiyuu_db[sl.seiyuu_idx[character[0]]].name, character[0]))

def main():
    import sys
    anime_id = sys.argv[1]
    list_seiyuu_anime(anime_id)
    return

if __name__ == '__main__':
    main()
