import sys

anime_db = []
anime_inv = {}
anime_order = []
anime_idx = {}

class Anime:
    def __init__(self, title, id, characters = list()):
        self.title = title
        self.id = id
        self.characters = characters[:]
    def write(self):
        print(self.title)
        print(self.id)
        for seiyuu_id, character_name in self.characters:
            print('{} {}'.format(seiyuu_id, character_name))
        print()

def read_anime(blob):
    blob = blob.split('\n')
    title = blob[0]
    id = blob[1]
    characters = []
    for character in blob[2:]:
        seiyuu_id = character.split()[0]
        character_name = character[len(seiyuu_id) + 1:]
        characters.append((seiyuu_id, character_name))
    return Anime(title, id, characters)

def add_anime(id, title, characters):
    if not update_anime_order(id):
        return False
    get_anime_db()
    anime = Anime(title, id, characters)
    anime_db.append(anime)
    anime_inv[id] = len(anime_db) - 1
    anime_order.append(id)
    anime_idx[id] = len(anime_order) - 1
    update_anime_db()
    return True

def get_anime_order():
    sys.stdin = open('db/anime_order', 'r')
    n = int(input() + input())
    for i in range(n):
        id = input()
        anime_order.append(id)
        anime_idx[id] = i
    sys.stdin.close()

def get_anime_db():
    sys.stdin = open('db/anime', 'r')
    arr = sys.stdin.read().split('\n\n')
    sys.stdin.close()
    
    n = int(arr[0])
    for i in range(n):
        anime_db.append(read_anime(arr[i+1]))
        anime_inv[anime_db[-1].id] = i
    sys.stdin.close()

def update_anime_order(anime_id):
    global anime_order
    
    get_anime_order()
    
    new_order = []
    is_new = True
    for id in anime_order:
        if id == anime_id:
            is_new = False
            continue
        new_order.append(id)
    new_order.append(anime_id)
    
    anime_order = new_order
    
    sys.stdout = open('db/anime_order', 'w')
    print(len(anime_order))
    print()
    for id in anime_order:
        print(id)
    sys.stdout.close()
    
    return is_new

def update_anime_db():
    sys.stdout = open('db/anime', 'w')
    print(len(anime_db))
    print()
    for anime in anime_db:
        anime.write()
    sys.stdout.close()

def main():
    return

if __name__ == '__main__':
    main()
