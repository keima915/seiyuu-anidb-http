import anime_list as al
import seiyuu_list as sl

def list_seiyuu_all():
    sl.get_seiyuu_db()
    al.get_anime_db()
    al.get_anime_order()
    for seiyuu in sl.seiyuu_db:
        seiyuu.roles.sort(key=lambda role : al.anime_idx[role[0]])
    
    sl.seiyuu_db.sort(key=lambda seiyuu: (len(seiyuu.anime), al.anime_idx[seiyuu.roles[0][0]]), reverse=True)
    
    
    print('List of all seiyuu:')
    for seiyuu in sl.seiyuu_db:
        print()
        print(' {} ({})'.format(seiyuu.name, seiyuu.id))
        print(' {} ({})'.format(al.anime_db[al.anime_inv[seiyuu.roles[-1][0]]].title, seiyuu.roles[-1][0]))
        for anime_id, character_name in seiyuu.roles:
            if anime_id == seiyuu.roles[-1][0]:
                print(' {}'.format(character_name))

def main():
    list_seiyuu_all()
    return

if __name__ == '__main__':
    main()
