import anime_list as al

def list_anime(X=5):
    al.get_anime_order()
    al.get_anime_db()
    
    print('Most recent anime:')
    for i in range(min(X, len(al.anime_db))):
        print()
        print(' {} ({})'.format(al.anime_db[al.anime_inv[al.anime_order[~i]]].title, al.anime_order[~i]))

def main():
    return

if __name__ == '__main__':
    main()
