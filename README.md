# Seiyuu-AniDB-HTTP

Command line tool that uses AniDB's HTTP API for keeping a local database of seiyuu appearing in anime you have watched.

**Commands:**
- List seiyuu and their most recently watched roles, sorted by how many anime the seiyuu appears in

    `python /path/to/seiyuu.py list_seiyuu`
- List seiyuu in an anime, sorted by how many anime the seiyuu appears in

    `python /path/to/seiyuu.py list_seiyuu <AniDB anime ID>`
- List roles of a seiyuu, sorted by most recently watched

    `python /path/to/seiyuu.py list_roles <AniDB seiyuu ID>`
- List X most recently watched anime (default is X=5)

    `python /path/to/seiyuu.py list_anime [X]`
- Mark anime as most recently watched, and add it to database if it is new

    `python /path/to/seiyuu.py anime <AniDB anime ID>`
- Clear anime and seiyuu databases

    `python /path/to/seiyuu.py clear`
- Change client and clientver

    `python /path/to/seiyuu.py config`

**Needs:**
- Python 3
- wget
- 7-Zip (command line) 
- [AniDB](https://anidb.net/) account

# Setup
1. Log in to your AniDB account, go [here](https://anidb.net/perl-bin/animedb.pl?show=client), register a new project, go to your project, and add a new client (for API, choose HTTP API).
2. Run `seiyuu.py` (`python /path/to/seiyuu.py`) and input your client name and client version.
